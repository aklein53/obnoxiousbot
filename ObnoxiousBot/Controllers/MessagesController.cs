﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Microsoft.ProjectOxford.Vision;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ObnoxiousBot
{
	[BotAuthentication]
	public class MessagesController : ApiController
	{
		/// <summary>
		/// POST: api/Messages
		/// Receive a message from a user and reply to it
		/// </summary>
		public virtual async Task<HttpResponseMessage> Post([FromBody]Activity activity)
		{
			if (activity != null)
			{
				switch (activity.GetActivityType())
				{
					case ActivityTypes.Message:
						// return our reply to the user
						await Conversation.SendAsync(activity, () => new EchoDialog());
						break;

					case ActivityTypes.ConversationUpdate:
					case ActivityTypes.ContactRelationUpdate:
					case ActivityTypes.Typing:
					case ActivityTypes.DeleteUserData:
					default:
						Trace.TraceError($"Unknown activity type ignored: {activity.GetActivityType()}");
						break;
				}
			}

			return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
		}

		private Activity HandleSystemMessage(Activity message)
		{
			if (message.Type == "Ping")
			{
				Activity reply = message.CreateReply();
				reply.Type = "Ping";
				return reply;
			}
			else if (message.Type == "DeleteUserData")
			{
				// Implement user deletion here
				// If we handle user deletion, return a real message
			}
			else if (message.Type == "BotAddedToConversation")
			{
			}
			else if (message.Type == "BotRemovedFromConversation")
			{
			}
			else if (message.Type == "UserAddedToConversation")
			{
			}
			else if (message.Type == "UserRemovedFromConversation")
			{
			}
			else if (message.Type == "EndOfConversation")
			{
			}

			return null;
		}
	}

	[Serializable]
	public class EchoDialog : IDialog
	{
		private string lastMessage = "";

		public async Task StartAsync(IDialogContext context)
		{
			context.Wait(MessageReceivedAsync);
		}

		public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> argument)
		{
			var message = await argument;

			foreach (var x in message.Attachments)
			{
				if (x.ContentType.Contains("image"))
				{
					HttpClient client = new HttpClient();
					var content = await client.GetAsync(x.ContentUrl);
					var stream = await content.Content.ReadAsStreamAsync();

					VisionServiceClient visionClient = new VisionServiceClient("f794d737d5f2489a8fb98ccd680c63df");
					var description = await visionClient.DescribeAsync(stream);

					if (description.Description.Captions.Count() > 0)
					{
						await context.PostAsync($" I see {description.Description.Captions[0].Text}.");
					}
					else
					{
						await context.PostAsync($"Wtf am I looking at?");
					}

					if (description.Description.Captions.Count() > 1)
					{
						await context.PostAsync($"Or maybe {description.Description.Captions[description.Description.Captions.Count() - 1].Text}. Fuck, I don't know.");
					}
				}
			}

			var messageText = message.Text.Trim().ToLower();
			messageText = new string(messageText.ToCharArray().Where(c => !char.IsPunctuation(c)).ToArray());

			if (messageText.Equals("what") ||
				messageText.Equals("wat"))
			{
				await context.PostAsync(("Er, you said: \"" + lastMessage + "\"").ToUpper());
			}
			else
			{
				lastMessage = message.Text;
			}

			context.Wait(MessageReceivedAsync);
		}
	}
}